this repo contain both client and server api projec for alpha abc test
to know how to run the client and server
go to the respective folder and read the readme.md

i have added two types of datasources.
1. In memory with h2 database for simply testing 
    you can see the line in the server resource folder for h2 database configuration.
        spring.datasource.url=jdbc:h2:mem:testdb
        spring.datasource.driverClassName=org.h2.Driver
        spring.datasource.username=sa
        spring.datasource.password=password
        spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    If you want to use the mysql db, comment the lines that is shown above abd uncomment below lines
        #spring.jpa.hibernate.ddl-auto=update
        #spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/db_alphaabc
        #spring.datasource.username=root
        #spring.datasource.password=root
I have added the dependancies for both my sql and h2 database in pom.xml.
You can comment one when it is not in use.Now commented Mysql 
        <!--added for my sql conneciton-->
		<!--<dependency>-->
			<!--<groupId>mysql</groupId>-->
			<!--<artifactId>mysql-connector-java</artifactId>-->
			<!--<scope>runtime</scope>-->
		<!--</dependency>-->
<!--h2 in  memory db for simplicity-->
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>
To run the server apis,
 Just go inside the folder `serverapi`
 run the command `mvn clean install spring-boot:run`
 This will make the server api's up and running.
 
Then to run client
 go to the folder `alphaabcclient`
 then run `npm install`
 then run `npm start`

 Then go to `localhost:3000`

 The application will be running

 
