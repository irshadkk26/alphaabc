import React, { useState } from "react";
import logo from "./logo.svg";
import { Button, DatePicker, version, message } from "antd";
import "antd/dist/antd.css";
import "./App.css"; 
import RevisionComponent from "./components/RevisionComponent";
import Header from "./components/shared/Header";

function App() {
  const [date, setDate] = useState(null);
  const handleChange = (value) => {
    message.info(
      `Selected Date: ${value ? value.format("YYYY-MM-DD") : "None"}`
    );
    setDate(value);
  };
  return (
    <div>
      <Header />
      <div className="App">
        <RevisionComponent/>
      </div>
    </div>
  );
}

export default App;
