import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { RevisionService } from "../services/revision.service";
import "antd/dist/antd.css";
import "./index.css";
import moment from "moment";
import {
  Form,
  Input,
  Tooltip,
  Select,
  Button,
  DatePicker,
  Table,
  Tag,
  Space,
  message,
} from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";

const { Option } = Select;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RevisionComponent = () => {
  const [form] = Form.useForm();
  //
  const [data, setData] = useState([]);

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    RevisionService.getAllRevisions().then((data) => {
      // console.log(JSON.stringify(data));
      setData(data);
    });
  }, []);

  const columns = [
    {
      title: "Revision Number",
      dataIndex: "revisionNumber",
      key: "revisionNumber",
    },
    {
      title: "Project Name",
      dataIndex: "projectName",
      key: "projectName",
    },
    {
      title: "Initiated UserName",
      dataIndex: "initiatedUserName",
      key: "initiatedUserName",
    },
    {
      title: "Change Details",
      dataIndex: "changeDetails",
      key: "changeDetails",
    },
    {
      title: "Approved By",
      dataIndex: "approvedBy",
      key: "approvedBy",
    },
    {
      title: "Approved Date",
      dataIndex: "approvedDate",
      key: "approvedDate",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <a
            onClick={() => {
              form.setFieldsValue({
                revisionNumber: record.revisionNumber,
                projectName: record.projectName,
                initiatedUserName: record.initiatedUserName,
                changeDetails: record.changeDetails,
                approvedBy: record.approvedBy,
                approvedDate: moment(record.approvedDate),
              });
            }}
          >
            Update
          </a>
          <a
            onClick={() => {
              RevisionService.deleteRevision(record.revisionNumber).then(
                (resp) => {
                  if (resp) {
                    setData(resp);
                    message.info(`Record deleted successfully`);
                  }
                  throw resp;
                }
              );
            }}
          >
            Delete
          </a>
        </Space>
      ),
    },
  ];
  const onFinish = (values) => {
    let params = values;
    params.approvedDate = values.approvedDate.toISOString();
    RevisionService.postRevision(params).then((resp) => {
      if (resp) {
        form.resetFields();
        setData(resp);
        message.info(`Record Added/Updated successfully`);
      }
      throw resp;
    });
  };

  const config = {
    rules: [
      {
        type: "object",
        required: true,
        message: "Please select time!",
      },
    ],
  };
  const { TextArea } = Input;
  return (
    <div className="wrapper">
      <div className="addrev">
        <Form
          {...formItemLayout}
          form={form}
          name="register"
          onFinish={onFinish}
          initialValues={{
            residence: ["zhejiang", "hangzhou", "xihu"],
            prefix: "86",
          }}
          scrollToFirstError
        >
          <Form.Item
            name="revisionNumber"
            label={
              <span>
                Revision Number&nbsp;
                <Tooltip title="Revision Number">
                  <QuestionCircleOutlined />
                </Tooltip>
              </span>
            }
          >
            <Input readOnly />
          </Form.Item>
          <Form.Item
            name="projectName"
            label={
              <span>
                Project Name&nbsp;
                <Tooltip title="Enter the project name">
                  <QuestionCircleOutlined />
                </Tooltip>
              </span>
            }
            rules={[
              {
                required: true,
                message: "Please input your project name!",
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="initiatedUserName"
            label={
              <span>
                Initiated User Name&nbsp;
                <Tooltip title="Enter the initiator name">
                  <QuestionCircleOutlined />
                </Tooltip>
              </span>
            }
            rules={[
              {
                required: true,
                message: "Please input your initiated user name!",
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="changeDetails"
            label={
              <span>
                Change Details&nbsp;
                <Tooltip title="Enter the change details">
                  <QuestionCircleOutlined />
                </Tooltip>
              </span>
            }
            rules={[
              {
                required: true,
                message: "Please input change details!",
                whitespace: true,
              },
            ]}
          >
            <TextArea />
          </Form.Item>
          <Form.Item
            name="approvedBy"
            label={
              <span>
                Approved By&nbsp;
                <Tooltip title="Enter who approved this revision">
                  <QuestionCircleOutlined />
                </Tooltip>
              </span>
            }
            rules={[
              {
                required: true,
                message: "Please input approver details!",
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name="approvedDate" label="DatePicker" {...config}>
            <DatePicker />
          </Form.Item>

          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
              + Revision
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div className="listrev">
        <Table columns={columns} dataSource={data} />
      </div>
    </div>
  );
};

export default RevisionComponent;
