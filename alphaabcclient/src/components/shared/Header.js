import React, { useState } from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./index.css";
import { PageHeader } from "antd";
class HeaderComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { revisions: [] };
  }
  componentDidMount() {}
  render() {
    return (
      <PageHeader
        className="site-page-header" 
        title="Alpha ABC"
        subTitle="Alpha ABC"
      />
    );
  }
}

export default HeaderComponent;
