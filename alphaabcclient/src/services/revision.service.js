import { APIUrlConstants } from "../util/apiurl.constant";
import { APIService } from "../util/api.service";
import { trackPromise } from "react-promise-tracker";

export class RevisionService {
  static getAllRevisions() {
    let url = APIUrlConstants.getApiUrl("apiUrl");
    return APIService.doGet(url, false).then((resp) => {
      if (resp) {
        return resp;
      }
      throw resp;
    });
  }
  static postRevision(input) {
    let url = APIUrlConstants.getApiUrl("apiUrl");
    return APIService.doPost(url, input, {}, false).then((resp) => {
      if (resp) {
        return resp;
      }
      throw resp;
    });
  }
  static deleteRevision(id) {
    let url = APIUrlConstants.getApiUrl("apiUrl") + "/delete/" + id;
    return APIService.doGet(url, false).then((resp) => {
      if (resp) {
        return resp;
      }
      throw resp;
    });
  }
}
