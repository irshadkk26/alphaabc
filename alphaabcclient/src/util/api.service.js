import { trackPromise } from "react-promise-tracker";

export class APIService {
  static doGet(url, accessToken = true) {
    return trackPromise(
      fetch(url, {
        method: "get",
        headers: new Headers({
          "Content-Type": "application/json",
        }),
      })
        .then((resp) => {
          const errorResponse = {
            network_error: true,
            status_code: 500,
            status_message: "Something went wrong in network",
          };
          if (resp.ok) {
            return resp.json();
          }

          throw errorResponse;
        })
        .catch((error) => {})
    );
  }

  static doPost(url, payload, headers = {}) {
    headers["Content-Type"] = "application/json";
    headers["Accept"] = "*";
    headers["Access-Control-Allow-Origin"] = "*";

    payload = payload || {};
    const conf = {
      method: "post",
      headers: headers,
      mode: "cors",
      datatype: "JSONP",
      body: JSON.stringify(payload),
    };
    return trackPromise(
      fetch(url, conf)
        .then((resp) => {
          const errorResponse = {
            network_error: true,
            status_code: 500,
            status_message: "Something went wrong in network",
          };
          if (resp.ok) {
            return resp.json();
          }
          throw errorResponse;
        })
        .catch((error) => {})
    );
  }
}
