export class APIUrlConstants {
  static API_URLS = {
    apiUrl: "http://localhost:8080/revisions",
  };
  static getApiUrl(key) {
    return this.API_URLS[key];
  }
}
