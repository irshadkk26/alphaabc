# alphaabc

Java spring boot project

Set Up for running

Clone the repo : `https://gitlab.com/irshadkk26/alphaabc.git`

Install MySQL: 
 configure the below properties in `application.properties`

spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/db_alphaabc
spring.datasource.username=root
spring.datasource.password=root

Run:
`mvn clean install spring-boot:run`

