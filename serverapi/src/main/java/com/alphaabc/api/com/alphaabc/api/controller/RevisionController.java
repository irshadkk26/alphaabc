package com.alphaabc.api.com.alphaabc.api.controller;

import java.util.List;

import com.alphaabc.api.com.alphaabc.api.model.Revision;
import com.alphaabc.api.com.alphaabc.api.service.RevisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/revisions")
public class RevisionController {
    @Autowired
    RevisionService service;

    /*
        method for getting all revisions
        @ created by Irshad on 31-may-2020
     */
    @GetMapping
    public ResponseEntity<List<Revision>> getAllRevisions() {
        List<Revision> list = service.getAllRevisions();

        return new ResponseEntity<List<Revision>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    /*
    method for getting a revision with its id
    @ created by Irshad on 31-may-2020
     */
    @GetMapping("/{id}")
    public ResponseEntity<Revision> getRevisionById(@PathVariable("id") Long id)
            throws Exception {
        Revision entity = service.getRevisionById(id);

        return new ResponseEntity<Revision>(entity, new HttpHeaders(), HttpStatus.OK);
    }

    /*
        method for getting updating or creating a new revision
        @ created by Irshad on 31-may-2020
     */
    @PostMapping
    public ResponseEntity<List<Revision>> createOrUpdateRevision(@RequestBody Revision revision)
            throws Exception {
        service.createOrUpdateRevision(revision);

        List<Revision> list = service.getAllRevisions();

        return new ResponseEntity<List<Revision>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    /*
        method for getting deleting a revision by its id
        @ created by Irshad on 31-may-2020
     */
    @GetMapping("delete/{id}")
    public ResponseEntity<List<Revision>> deleteRevisionById(@PathVariable("id") Long id)
            throws Exception {
        service.deleteRevisionById(id);
        List<Revision> list = service.getAllRevisions();

        return new ResponseEntity<List<Revision>>(list, new HttpHeaders(), HttpStatus.OK);
    }

}