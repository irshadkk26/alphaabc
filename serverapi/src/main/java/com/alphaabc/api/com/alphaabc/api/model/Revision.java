package com.alphaabc.api.com.alphaabc.api.model;

import javax.persistence.*;
import java.util.Date;


/*
The revision table entity
JPA maps this entitiy to database table TBL_REVISIONS
 */
@Entity
@Table(name = "TBL_REVISIONS")
public class Revision {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long revisionNumber;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "initiated_user_name")
    private String initiatedUserName;

    @Column(name = "change_details")
    private String changeDetails;

    @Column(name = "approved_by")
    private String approvedBy;

    @Column(name = "approved_date")
    private Date approvedDate;

    //Setters and getters

    public Long getRevisionNumber() {
        return revisionNumber;
    }

    public void setRevisionNumber(Long revisionNumber) {
        this.revisionNumber = revisionNumber;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getInitiatedUserName() {
        return initiatedUserName;
    }

    public void setInitiatedUserName(String initiatedUserName) {
        this.initiatedUserName = initiatedUserName;
    }

    public String getChangeDetails() {
        return changeDetails;
    }

    public void setChangeDetails(String changeDetails) {
        this.changeDetails = changeDetails;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }
}