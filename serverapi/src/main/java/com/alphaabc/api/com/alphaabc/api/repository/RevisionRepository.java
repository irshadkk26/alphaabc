package com.alphaabc.api.com.alphaabc.api.repository;

import com.alphaabc.api.com.alphaabc.api.model.Revision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/*
The Repository class for performing CRUD operation on Revision entity
 */
@Repository
public interface RevisionRepository extends JpaRepository<Revision, Long> {

}