package com.alphaabc.api.com.alphaabc.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.alphaabc.api.com.alphaabc.api.model.Revision;
import com.alphaabc.api.com.alphaabc.api.repository.RevisionRepository;
import com.alphaabc.api.com.alphaabc.api.serviceImpl.RevisionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
Service class for CRUD operation on Revision entity
 */
@Service
public class RevisionService implements RevisionServiceImpl {

    @Autowired
    RevisionRepository repository;

    /*
        method for getting all revisions
        @ created by Irshad on 31-may-2020
     */
    @Override
    public List<Revision> getAllRevisions() {
        List<Revision> revisionList = repository.findAll();

        if (revisionList.size() > 0) {
            return revisionList;
        } else {
            return new ArrayList<Revision>();
        }
    }

    /*
        method for getting a revision with its id
        @ created by Irshad on 31-may-2020
         */
    @Override
    public Revision getRevisionById(Long id) throws Exception {
        Optional<Revision> revision = repository.findById(id);

        if (revision.isPresent()) {
            return revision.get();
        } else {
            throw new Exception("No Revision record exist for given id");
        }
    }

    /*
            method for getting updating or creating a new revision
            @ created by Irshad on 31-may-2020
         */
    @Override
    public Revision createOrUpdateRevision(Revision entity) throws Exception {
//        checks for revision number and if found checks if that particular revision exists
//        if exists,then update the record else add new record
        if (null != entity.getRevisionNumber()) {
            Optional<Revision> revision = repository.findById(entity.getRevisionNumber());

            if (revision.isPresent()) {
                Revision newEntity = revision.get();
                newEntity.setProjectName(entity.getProjectName());
                newEntity.setInitiatedUserName(entity.getInitiatedUserName());
                newEntity.setChangeDetails(entity.getChangeDetails());
                newEntity.setApprovedBy(entity.getApprovedBy());
                newEntity.setApprovedDate(entity.getApprovedDate());

                newEntity = repository.save(newEntity);

                return newEntity;
            } else {
                entity = repository.save(entity);

                return entity;
            }
        } else {
            entity = repository.save(entity);

            return entity;
        }


    }

    /*
            method for getting deleting a revision by its id
            @ created by Irshad on 31-may-2020
         */
    @Override
    public void deleteRevisionById(Long id) throws Exception {
        Optional<Revision> revision = repository.findById(id);

        if (revision.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new Exception("No Revision record exist for given id");
        }
    }
}