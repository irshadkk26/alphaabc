package com.alphaabc.api.com.alphaabc.api.serviceImpl;

import com.alphaabc.api.com.alphaabc.api.model.Revision;

import java.util.List;

public interface RevisionServiceImpl {
    /*
        method for getting all revisions
        @ created by Irshad on 31-may-2020
     */
    public List<Revision> getAllRevisions();

    /*
        method for getting a revision with its id
        @ created by Irshad on 31-may-2020
         */
    public Revision getRevisionById(Long id) throws Exception;

    /*
            method for getting updating or creating a new revision
            @ created by Irshad on 31-may-2020
         */
    public Revision createOrUpdateRevision(Revision entity) throws Exception;

    /*
        method for getting deleting a revision by its id
        @ created by Irshad on 31-may-2020
     */
    public void deleteRevisionById(Long id) throws Exception;
}
